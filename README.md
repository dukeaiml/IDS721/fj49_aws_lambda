# Rust Lambda Data Processor

## Overview

This project implements an AWS Lambda function in Rust using Cargo Lambda. The Lambda function receives a JSON payload containing three integers, calculates their sum, product, and average, and returns the results in JSON format. The processed data can be triggered via API Gateway.

## Requirements

- Rust compiler and Cargo package manager installed.
- AWS account with permission to create Lambda functions.
- API Gateway endpoint for triggering the Lambda function.

## Project Structure

1. **Lambda Function Code:**
   - Create a Rust project using Cargo.
   - Add dependencies for AWS Lambda and serde for JSON serialization.
   - Implement the Lambda function logic to receive three integers, process them, and return the results.
   
2. **API Gateway Integration:**
   - Configure an API Gateway endpoint to trigger the Lambda function.
   - Set up the necessary permissions and roles for the API Gateway to invoke the Lambda function.
   
3. **Data Processing:**
   - Define the sample data format that the Lambda function will receive (e.g., {"a": 10, "b": 20, "c": 30}).
   - Process the incoming data to calculate the sum, product, and average of the three integers.
   - Ensure error handling for invalid input or other runtime errors.
   
4. **Documentation:**
   - Create a README.md file explaining the project's purpose, setup instructions, usage guidelines, and any other relevant information.
   - Include documentation within the code to explain the function's logic and any important considerations.

## Setup Instructions

1. Clone the repository:

   ```
   git clone https://gitlab.com/dukeaiml/IDS721/fj49_aws_lambda.git
   ```

2. Navigate to the project directory:

   ```
   cd fj49_aws_lambda
   ```

3. Build the project:

   ```
   cargo build --release
   ```

## Usage

- **Triggering the Lambda Function:**
  - Configure an API Gateway endpoint to trigger the Lambda function.
  - Send a JSON payload containing three integers (e.g., {"a": 10, "b": 20, "c": 30}) to the API Gateway endpoint.
  - The Lambda function will calculate the sum, product, and average of the input integers and return the results in JSON format.


## Screenshots 

### Deployed lambda function on AWS

![Screenshot 1](screenshots/deployed.png)

### Input payload
![Screenshot 1](screenshots/input_payload.png)

### Response (output) payload

![Screenshot 1](screenshots/output_payload.png)


-----

## License

This project is licensed under the [MIT License](LICENSE).


